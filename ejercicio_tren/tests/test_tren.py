from app.tren import *
import pytest

def crear_tren_con_locomotora(potencia):
    locomotora = Locomotora(potencia)
    tren = Tren(locomotora)
    return tren

def agregar_vagones(tren, cantidad_de_vagones):
    for i in range(cantidad_de_vagones):
        tren.agregar(Vagon(10,10))

def test_vagon_tiene_capacidad():
    capacidad_pasajeros_sentados = 10
    capacidad_pasajeros_parados = 10
    vagon = Vagon(capacidad_pasajeros_sentados, capacidad_pasajeros_parados)
    assert vagon.capacidad() == 20

def test_vagon_tiene_capacidad_numero_positivo():
    with pytest.raises(ValueError):
        Vagon(-1, -1)

def test_locomotora_tiene_potencia():
    potencia = 1000
    locomotora = Locomotora(potencia)
    assert locomotora.potencia() == potencia

def test_locomotora_XXXXX():
    with pytest.raises(PotenciaInvalidaError):
        Locomotora(potencia=3000)

def test_locomotora_con_potencia_1000_hasta_4_vagones():
    assert Locomotora(1000).cantidad_max_vagones() == 4

def test_locomotora_con_potencia_2000_hasta_7_vagones():
    assert Locomotora(2000).cantidad_max_vagones() == 7

def test_tren_capacidad_con_un_vagon():
    locomotora = Locomotora(potencia=1000)
    vagon = Vagon(10, 10)
    tren = Tren(locomotora)
    tren.agregar(vagon)
    assert tren.capacidad() == 20

def test_tren_capacidad_con_varios_vagones():
    tren = crear_tren_con_locomotora(1000)
    tren.agregar(Vagon(10, 10))
    tren.agregar(Vagon(10, 10))
    tren.agregar(Vagon(5, 15))
    assert tren.capacidad() == 60

def test_tren_con_potencia_1000_hasta_4_vagones():
    tren = crear_tren_con_locomotora(1000)
    agregar_vagones(tren, 4)
    with pytest.raises(DemasiadosVagonesError):
        tren.agregar(Vagon(10, 10))

def test_tren_con_potencia_2000_hasta_7_vagones():
    tren = crear_tren_con_locomotora(2000)
    agregar_vagones(tren, 7)
    with pytest.raises(DemasiadosVagonesError):
        tren.agregar(Vagon(10, 10))

# validaciones: cantidad de pasajeros en vagon
# refactor de la responsabilidad de cantidad_max_de_vagones
