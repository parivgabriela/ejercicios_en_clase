from datetime import date
from app.app import app, Calculadora


def test_get_calculadora():
    response = app.test_client().get('/')
    assert response.status_code == 200

def test_post_sumar():
    response = app.test_client().post('/sumar', data=dict(
        x=2,
        y=2
    ))
    assert response.status_code == 200
    assert "2 + 2 = 4" in response.get_data(as_text=True)

def test_sumar_0_devuelve_codigo_400():
    response = app.test_client().post('/sumar', data=dict(
        x=0,
        y=2
    ))
    assert response.status_code == 400
    #assert "2 + 2 = 4" in response.get_data(as_text=True)

def test_calculadora():
    assert Calculadora().sumar(1,1) == 2
