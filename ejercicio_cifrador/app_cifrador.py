import sys
from app.cifrador import Cifrador

corrimiento = int(sys.argv[1])
texto = sys.argv[2]

cifrador = Cifrador(corrimiento)
resultado = cifrador.cifrar(texto)

print("Resultado: {}".format(resultado))
