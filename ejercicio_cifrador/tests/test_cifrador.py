from app.cifrador import Cifrador

def test_corrimiento_cero():
    cifrador = Cifrador(0)
    assert cifrador.cifrar("hola") == "hola"


"""
def test_corrimiento_1():
    cifrador = Cifrador(1)
    assert cifrador.cifrar("abc") == "bcd"


def test_corrimiento_al_final_del_alfabeto():
    cifrador = Cifrador(1)
    assert cifrador.cifrar("yz") == "za"


def test_corriemiento_vuelta_completa():
    cifrador = Cifrador(27)
    assert cifrador.cifrar("ab") == "ab"
"""
