class Alquiler:

    def facturar(self):
        importe_bruto = self.calcular_importe_bruto()
        importe_bruto = self.aplicar_descuento(importe_bruto)
        return importe_bruto

    def aplicar_descuento(self, importe_bruto):
        if (self._cuit_cliente.startswith("26")):
            importe_bruto = importe_bruto - (importe_bruto * 0.05)
        return importe_bruto

class AlquilerPorHora(Alquiler):

    costo_por_hora = 100

    def __init__(self, cuit, horas):
        self._horas = horas
        self._cuit_cliente = cuit

    def calcular_importe_bruto(self):
        return self._horas * AlquilerPorHora.costo_por_hora


class AlquilerPorDia(Alquiler):

    costo_por_dia = 2000

    def __init__(self, cuit, dias):
        self._dias = dias
        self._cuit_cliente = cuit

    def calcular_importe_bruto(self):
        return self._dias * AlquilerPorDia.costo_por_dia

