from app.cantidad import Cantidad
import pytest


def test_cantidad_no_puede_ser_negativa():
    with pytest.raises(ValueError):
        Cantidad(-1)


def test_cantidad_debe_ser_entero():
    with pytest.raises(ValueError):
        Cantidad(1.5)


def test_si_dos_cantidad_representan_lo_mismo_son_igual():
    assert Cantidad(1) == Cantidad(1)
