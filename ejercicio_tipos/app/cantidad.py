class Cantidad:

    def __init__(self, numero):
        self.asegurar_numero_entero(numero)
        self.asegurar_numero_es_positivo(numero)
        self.numero = numero


    def asegurar_numero_entero(self, numero):
        if not isinstance(numero, int):
            raise ValueError("La cantidad debe ser entero")


    def asegurar_numero_es_positivo(self, numero):
        if numero < 0:
            raise ValueError("La cantidad no puede ser negativa")

    def __eq__(self, otra_cantidad):
        return self.numero == otra_cantidad.numero
