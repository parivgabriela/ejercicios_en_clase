from app.persona import Persona
import pytest

def test_persona_no_tiene_repetido():
    Persona("juan")
    with pytest.raises(ValueError):
        Persona("juan")


def test_persona_nombre_esta_disponble_sino_esta_utilizado():
    Persona("pedro")
    assert Persona.esta_disponible("pedro") is False
    assert Persona.esta_disponible("maria") is True

def test_persona_nombre_no_puede_ser_vacio():
    with pytest.raises(ValueError):
        Persona("")
