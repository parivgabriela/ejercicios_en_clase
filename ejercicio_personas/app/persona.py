class Persona:

    nombres_utilizados = []

    def __init__(self, nombre):
        self.asegurar_nombre(nombre)
        if(nombre in self.nombres_utilizados):
            raise ValueError("El nombre indicado no esta disponible")
        self.nombres_utilizados.append(nombre)
        self.__nombre = nombre

    def nombre(self):
        return self.__nombre

    @classmethod
    def esta_disponible(cls, nombre):
        return not (nombre in cls.nombres_utilizados)

    @staticmethod
    def asegurar_nombre(nombre):
        if nombre == None or nombre == "":
            raise ValueError("Nombre no pueder vacio")
