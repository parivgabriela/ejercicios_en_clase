# pip3 install --user pytest
import pytest

from app.cuenta import Cuenta
# Cuenta
#   saldo()      => es cero cuando se crea
#   depositar(x) => incrementa al saldo en x
#   retirar(x)   => decrementa el saldo en x,
#                => lanza ValueError si x > saldo

def test_cuenta_se_crea_con_saldo_cero():
    cuenta = Cuenta()
    assert cuenta.saldo() == 0

def test_cuenta_depositar_incremental_el_saldo():
    cuenta = Cuenta()
    cuenta.depositar(10)
    assert cuenta.saldo() == 10

def test_cuenta_retirar_decrementa_el_saldo():
    cuenta = Cuenta()
    cuenta.depositar(10)
    cuenta.retirar(5)
    assert cuenta.saldo() == 5

def test_cuenta_retirar_lanza_error_si_retiro_mayor_a_saldo():
    cuenta = Cuenta()
    cuenta.depositar(10)
    with pytest.raises(ValueError):
        cuenta.retirar(50)

