class Cuenta:

    todas_las_cuentas = []

    def __init__(self):
        self.__saldo = 0

    def saldo(self):
        return self.__saldo

    def depositar(self, monto):
        self.__saldo += monto

    def retirar(self, monto):
        if monto > self.__saldo:
            raise ValueError("No tenes tanta plata")
        self.__saldo -= monto
